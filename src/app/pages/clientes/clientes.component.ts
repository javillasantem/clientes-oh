import { Component, OnInit } from '@angular/core';
import { ClienteModel } from 'src/app/models/cliente.model';
import { ClientesService } from 'src/app/services/clientes.service';
import Swal from 'sweetalert2';

@Component({
    selector: 'app-clientes',
    templateUrl: './clientes.component.html',
    styleUrls: ['./clientes.component.css']
})
export class ClientesComponent implements OnInit {
    clientes: ClienteModel[] = [];
    cargando = false;
    edadPromedio: number = 0;

    constructor(private clientesService: ClientesService) { }

    ngOnInit(): void {
        this.cargando = true;

        this.clientesService.getClientes().subscribe(
            resp => {
                this.clientes = resp.data;
                this.edadPromedio = Number(resp.edadPromedio);
                this.cargando = false;
            }
        );
    }

    borrarCliente(cliente: ClienteModel, i: number) {
        Swal.fire({
            title: '¿Está seguro?',
            text: `Está seguro que desea borrar a ${ cliente.nombre }`,
            // type: 'question',
            showConfirmButton: true,
            showCancelButton: true
        }).then(resp => {
            if (resp.value) {
                this.clientes.splice(i, 1);
                this.clientesService.borrarCliente(cliente.id).subscribe();
            }
        });
    }
}
