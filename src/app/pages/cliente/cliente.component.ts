import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs';
import { ClientesService } from 'src/app/services/clientes.service';
import { ClienteModel } from 'src/app/models/cliente.model';
import Swal from 'sweetalert2';

@Component({
    selector: 'app-cliente',
    templateUrl: './cliente.component.html',
    styleUrls: ['./cliente.component.css']
})
export class ClienteComponent implements OnInit {
    cliente: ClienteModel = new ClienteModel();

    constructor(
        private clientesService: ClientesService,
        private route: ActivatedRoute) { }
  
    ngOnInit() {
        const id = this.route.snapshot.paramMap.get('id');

        if (id !== 'nuevo') {
            this.clientesService.getCliente(id).subscribe(
                (resp: ClienteModel) => {
                    this.cliente = resp;
                    this.cliente.id = id;
                }
            );
        } else {
            this.cliente.edad = 0;
        }
    }
  
    guardar(form: NgForm) {
        if (form.invalid) {
            Swal.fire({
                title: "Atención",
                text: "Faltan completar datos del formulario"
            });
            return;
        }

        Swal.fire({
            title: 'Espere',
            text: 'Guardando información',
            allowOutsideClick: false
        });

        Swal.showLoading();
        let peticion: Observable<any>;

        if ( this.cliente.id ) {
            peticion = this.clientesService.actualizarCliente( this.cliente );
        } else {
            peticion = this.clientesService.crearCliente( this.cliente );
        }

        peticion.subscribe(resp => {
            Swal.fire({
                title: this.cliente.nombre,
                text: 'Se actualizó correctamente',
            });
        });
    }

    cambiarFechaNacimiento(fechaNacimiento) {
        var timeDiff = Math.abs(Date.now() - new Date(fechaNacimiento).getTime());
        this.cliente.edad = Math.floor(timeDiff / (1000 * 3600 * 24) / 365.25);
    }
}
