export class ClienteModel {
    id: string;
    nombre: string;
    apellido: string;
    edad?: number;
    fechaNacimiento: Date;

    constructor() {
    }
}
